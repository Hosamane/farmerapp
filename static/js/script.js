

 $('.selectize').selectize({
        sortField: 'text'
    });
$(document).ready(function(){
   

	function getCookie(name) {
    var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    });

    $('.dis').click(function(){
        var row = $(this).parents('.item-row');
        var pk = row.find('.sch_pk').text();
        var sch_freight = row.find('.sch_freight').text();
        $('#dis_freight').val(sch_freight)
        $('#schedule_id').val(pk)
        $('.disp_row').empty()


        $.ajax({
            url:"/scheduled_list/",
            type:"POST",
            data :{'sch_pk':pk},
            success:function(jsondata){
                console.log(jsondata)

            	$.each(jsondata["dispatchs_list"], function(key,value) {
            		var now_datetime = value.dis_date;
					var dis_date = new Date(now_datetime);
				  var htmltext = '<td><a href="/update_dispatch/'+ value.dispatch_head_pk+'"> Dis#'+ value.dispatch_head_pk+ '</a></td>'
                                  +'<td> '+ value.dis_qty+ '</td>'
                                  +'<td> '+ dis_date+ '</td>'
                                  +'<td> '+ value.status+ '</td>'
                 $('.disp_row').append('<tr class="dis_table">'+htmltext+'</tr>');
				});

                $('#contract_rate').val(jsondata['contractRate'])



            }          

        })
        
     })
    $('#vehicle_owner').change(function(){
        var ajvehicle_owner_pk = $(this).val()
        $.ajax({
            url:"/scheduled_list/",
            type:"POST",
            data:{"ajvehicle_owner_pk":ajvehicle_owner_pk},
            success:function(vehicles_list){
                console.log(vehicles_list)
                var vehicleslen = vehicles_list.length;
                for(i=0; i<vehicleslen; i++){
                    if(i==0)
                          {
                            $('.sel_vehicle').html('<select class="form-control selected vehicle_number" name="vehicle_num"><option value="">Select Vehicle</option><option value="'+vehicles_list[i]['pk']+'">'+vehicles_list[i]['vehicle_num']+'</option></select>');
                          }
                         else
                          {
                            $('.vehicle_number').append('<option value="'+vehicles_list[i]['pk']+'">'+vehicles_list[i]['vehicle_num']+'</option>');
                          }
                          
                }
                $('.vehicle_number').selectize({
                        sortField: 'text'
                    });
            }
        })
    })

    $('#payment_type').change(function(){
        $('.paymentdiv').hide()
        var ajpayment_type_pk = $(this).val()
        $.ajax({
            url:"/payment/",
            type:"POST",
            data:{'ajpayment_type_pk':ajpayment_type_pk},
            success:function(payTypeData){
                if (payTypeData['display_flag'] == true){
                    $('#id_payable_flag').prop('checked', false);
                    console.log('display_flag',payTypeData['display_flag'])
                    $('.paymentdiv').show()
                }
                else if(payTypeData['payable_flag']==true){
                    $('#id_payable_flag').prop('checked', true);
                    console.log('payable_flag',payTypeData['payable_flag'])
                }
                else if(payTypeData['payable_flag']==false){
                    $('#id_payable_flag').prop('checked', false);
                    console.log('payable_flag',payTypeData['payable_flag'])
                }
            }
        })
    });


//    $('#displant, #source').change(function(){
//        var source_id = $("#source").val()
//        var plant_id = $("#displant").val()
//        if (source_id && plant_id){
//            $.ajax({
//                url:"/new_schedule/",
//                type:"POST",
//                data:{"source_id":source_id,"plant_id":plant_id},
//                success:function(content){
//                    if(content['data']==false){
//
//                        $( ".modal" ).modal()
//
//
//
//
//
//
//
//
//                    }
//
//                }
//            })
//        }
//    })
//
})


$("#addrows").click(function(){
    for (i=1;i<=5;i++){
        $('.trows').append('<tr>\
                                <td><input type="text" value="0" name="itemqty[]" class="form-control"></td>\
                                <td><input type="date" name="date[]" class="form-control"></td>\
                            </tr>')
        }
})