from rest_framework.generics import ListAPIView
from rest_framework import generics,viewsets
from rest_framework.views import APIView
from rest_framework import *
from farmerSurvey.models import *
from serializers import *
from rest_framework.status import HTTP_200_OK,HTTP_400_BAD_REQUEST
from rest_framework.response import Response
from rest_framework.filters import (
	SearchFilter,
	OrderingFilter,
	DjangoFilterBackend
	)
from django.db.models import Count, Min, Sum, Avg
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from rest_framework.status import HTTP_200_OK,HTTP_400_BAD_REQUEST,HTTP_406_NOT_ACCEPTABLE

# class UserLoginAPIView(APIView):
# 	# permission_classes = [AllowAny]
# 	serializer_class = UserLoginSerializer

# 	def post(self,request,*args,**kwargs):
# 		data = request.data
# 		print data
# 		serializer = UserLoginSerializer(data=data)
# 		if serializer.is_valid(raise_exception=True):
# 			db_user = User.objects.get(username=data['username'])
# 			new_data = serializer.data
# 			if Member.objects.filter(profile__pk=db_user.pk).exists():
# 				member = Member.objects.get(profile__pk=db_user.pk)
# 				new_data['member_pk']=member.pk
# 				new_data['member_name']=member.name
# 				new_data['is_preferred']=member.is_preferred
# 				new_data['user_pk']=db_user.pk
# 			else:
# 				new_data['user_pk']=db_user.pk
# 				new_data['is_preferred']=True
# 			return Response(new_data,status=HTTP_200_OK)
# 		return Response(serializers.error,status=HTTP_400_BAD_REQUEST)


# class UserViewSet(viewsets.ModelViewSet):
#     queryset = User.objects.all()
#     serializer_class = UserSerializer

class InstitutionSer(viewsets.ModelViewSet):
	serializer_class = InstitutionLog
	queryset=Institution.objects.all()

class MemberViewSet(viewsets.ModelViewSet):
	serializer_class = MemberSerializer
	queryset=Member.objects.all()


class CropSer(viewsets.ModelViewSet):
	serializer_class = CropsLog
	queryset=Crop.objects.all()

class Water_sourceSer(viewsets.ModelViewSet):
	serializer_class = Water_sourceLog
	queryset=Water_source.objects.all()	

class FieldSer(viewsets.ModelViewSet):
	serializer_class = FarmerFieldsLog
	queryset=FarmerField.objects.all()
	
	
class PointsSer(viewsets.ModelViewSet):
	serializer_class = PointsLog
	queryset=Point.objects.all()

class VillageSer(viewsets.ModelViewSet):
	serializer_class = VillageLog
	queryset = Village.objects.all()


class FarmerDetailPost(APIView):

	def get(self,request,format=None):
		
		farmers = FarmerDetail.objects.all()
		data=[]
		for farmer in farmers:
			fields= FarmerField.objects.filter(farmerDetails__pk=farmer.pk)
			d={}
			d['pk']=farmer.pk
			d['name']=farmer.name
		
			field_count = len(fields)
			if len(fields) == 0:
				field_count = -1
			if farmer.no_of_fields > field_count:
				data.append(d)
		return Response(data,status=HTTP_200_OK)


	def post(self,request,*args,**kwargs):
		data = request.data
		
		name = data.get('name')
		mobile_num = data.get('mobile_num')
		district = data.get('district')
		former_code = data.get('former_code')
		village = data.get('village')
		
		db_farmerdetail = FarmerDetail.objects.create(
			name=name,
			mobile_num=mobile_num,
			district=district,
			former_code=former_code,
			village=Village.objects.get(village__pk=village)					
			)
		db_farmerdetail.save()	
		return Response('success',status=HTTP_200_OK)

class PointsPost(APIView):
	def post(self,request,*args,**kwargs):
		data = request.data
		print'data PointsPost',data

		acars = data['acres']
		water_source= data['water_source']
		farmerDetails= data['farmer_name']
		crop_name= data['crop_type']
		latlngs = data['pointses']
		village = data['village_pk']
		jsl1_distance=data['distJsl']
		jsl2_distance=data['distJkd']

		

		
		db_farmerfield = FarmerField.objects.create(
			acars=acars,
			town = "0" ,
			water_source=Water_source.objects.get(pk = water_source),
			farmerDetails = FarmerDetail.objects.get(pk = farmerDetails),
			crop_name=Crop.objects.get(pk = crop_name),
			village=Village.objects.get(pk=village),
			jsl1_distance=jsl1_distance,
			jsl2_distance=jsl2_distance
			)
		db_farmerfield.save()
		
		d=[]
		for latlng in latlngs:
			
			db_points = Point.objects.create(latitude = latlng['latitude'],longitude = latlng['longitude'] , farmerField = db_farmerfield)
			d.append(db_points)
			db_points.save()
		
		return Response('success',status=HTTP_200_OK)


class FarmerFieldPost(APIView):
	def post(self,request,*args,**kwargs):
		data = request.data
		print'farmerFieldPost',data
		acars = data.get('acars')
		town = data.get('town')
		water_source= data.get('water_source')
		farmerDetails= data.get('farmerDetails')		
		crop_name= data.get('crop_name')
		db_farmerfield = FarmerField.objects.create(
			acars=acars,
			town=town,
			water_source=water_source.objects.get(pk = water_source),
			farmerDetails = FarmerDetail.objects.get(pk = farmerDetails),
			crop_name=Crop.objects.get(pk = crop_name)
			
			)
		db_farmerfield.save()

		return Response('success',status=HTTP_200_OK)
	

class GetAllPointsByFields(APIView):
	def get(self,request,format=None):
		data = []
		
		
		fields = FarmerField.objects.all()
		for field in fields:
			fielddic={}
			fielddic['farmer']=field.farmerDetails.name
			fielddic['acres']=field.acars
			fielddic['water_source']=field.water_source.water_source
			fielddic['crop_name']=field.crop_name.crop_name

			points = Point.objects.filter(farmerField = field)
			list1=[]
			for point in points:
				dic={}
				dic['latitude']=point.latitude
				dic['longitude']=point.longitude
				list1.append(dic)
			fielddic['points']=list1
			data.append(fielddic)
		return Response(data,status=HTTP_200_OK)

class AddnewFieldsViewset(APIView):
	def post(self,request,*args,**kwargs):
		data = request.data

		farmers = FarmerDetail.objects.all()
		farmerfield = FarmerField.objects.all()

		crop=data['crop']
		watersource=data.get('watersource')
		latlngs = data['latlng']
		acars = data['acars']
		db_farmerfield = FarmerField.objects.create(
			acars = acars,
			town = "0" ,
			farmerDetails = FarmerDetail.objects.get(pk = data['farmers']),
			crop_name=Crop.objects.get(pk = crop),
			water_source=Water_source.objects.get(pk = watersource)
			)
		
		db_farmerfield.save()

		

		d=[]
		for latlng in latlngs:
			
			db_points = Point.objects.create(latitude = latlng['lat'],longitude = latlng['lng'] , farmerField = db_farmerfield)
			d.append(db_points)
			
		return Response('success',status=HTTP_200_OK)

class GetAllFieldsAcars(APIView):
	def get(self,request,format=None):
		data=[]
		farmers = FarmerDetail.objects.all()
		
		for farmer in farmers:
			farmerdata={}
			farmerdata['name']=farmer.name
			farmerdata['mobile_num']=farmer.mobile_num
			# farmerdata['village']=farmer.village
			farmerdata['district']=farmer.district
			farmerdata['id_proof']=farmer.id_proof

			fields= FarmerField.objects.filter(farmerDetails__pk=farmer.pk).aggregate(Sum('acars'))
			
			farmerdata['acars']=fields['acars__sum']
			no_of_fields= FarmerField.objects.filter(farmerDetails__pk=farmer.pk)
			farmerdata['no_of_fields']=len(no_of_fields)
			data.append(farmerdata)
		return Response(data,status=HTTP_200_OK)		
		return Response('success',status=HTTP_200_OK)



			







