from django.conf.urls import url,include
from django.contrib import admin
from farmerSurvey.api import views as api_views
from rest_framework import routers


router = routers.DefaultRouter()
# router.register(r'Farmerdetails', api_views.FarmerSer,base_name='FarmerDetail')
router.register(r'Points', api_views.PointsSer,base_name='LatLan')
router.register(r'Crops', api_views.CropSer,base_name='crops')
router.register(r'Water_source', api_views.Water_sourceSer,base_name='Water_source')
router.register(r'Fieldss', api_views.FieldSer,base_name='Fields')
router.register(r'Village', api_views.VillageSer,base_name='Village')
router.register(r'Institution', api_views.InstitutionSer,base_name='Institution')
# router.register(r'users', api_views.UserViewSet)
router.register(r'member', api_views.MemberViewSet)


urlpatterns = [
    
	
    url(r'^api/', include(router.urls)),
    # url(r'^api/login/',api_views.UserLoginAPIView.as_view(),name='login'),
    url(r'^api/PointsPost/',api_views.PointsPost.as_view()),
    url(r'^api/GetAllPointsByFields/',api_views.GetAllPointsByFields.as_view()),
    url(r'^api/FarmerDetailPost/',api_views.FarmerDetailPost.as_view()),
    url(r'^api/FarmerFieldPost/',api_views.FarmerFieldPost.as_view()),
    url(r'^api/AddnewFieldsViewset/',api_views.AddnewFieldsViewset.as_view()),
    url(r'^api/GetAllFieldsAcars/',api_views.GetAllFieldsAcars.as_view())


    
]
