# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.shortcuts import render
from models import *
from django.http import HttpResponseRedirect, HttpResponse, request
import json
import json as simplejson
from django.contrib.auth.models import *
from xlrd import open_workbook
import xlrd
from django.db.models import Count, Min, Sum, Avg
from django.contrib.auth import authenticate,logout
from django.contrib.auth import login as auth_login
from django.contrib import messages

@csrf_exempt
def login(request):
	if request.method == 'GET':
		return render(request,'login.html')
	elif request.method == 'POST':
		username = request.POST['username']
		password = request.POST['password']
		user = authenticate(username=username, password=password)
		if user is not None:
			if user.is_active:
				auth_login(request,user)
				messages.success(request,'%s logged In'%(user.username))
				return HttpResponseRedirect('/map/')
			else:
				print("The password is valid, but the account has been disabled!")
		else:
			messages.error(request,'The username and password were incorrect.')
			print("The username and password were incorrect.")
			return HttpResponseRedirect('/')

def logout_view(request):
	logout(request)
	return HttpResponseRedirect('/')


# @login_required(login_url='/')
def mapview(request):
	if request.method == "GET":
		farmers = FarmerDetail.objects.all()
		water_source=Water_source.objects.all()
		villages=Village.objects.all()
		institution = Institution.objects.all()
		
		
		dic2={}
		data = []
		
		ostr=""
		fields = FarmerField.objects.all()
		for field in fields:
			str1=""
			fielddic={}
			fielddic['farmer']=field.farmerDetails.name

			fielddic['acres']=str(field.acars)
			fielddic['crop']=field.crop_name.crop_name
			fielddic['distance1']=str(field.distance_from_institution)

			
			points = Point.objects.filter(farmerField = field)
			list1=[]
			str2=""
			for point in points:
				dic={}
				dic['latitude']=str(point.latitude)
				dic['longitude']=str(point.longitude)
				list1.append(dic)
			fielddic['points']=list1
			data.append(fielddic)
			
		js_data = simplejson.dumps(data)
		
		return render(request, "map.html",{'institution':institution,'village':villages,'water_source':water_source,'crop':Crop.objects.all(),'table_data': json.dumps(list(data))})



	elif request.is_ajax(): 
		village = request.POST.get('village')
		institution = request.POST.get('institution')
		print'eeeeeeeee',institution
		if village == None:
			villages = Village.objects.filter(institution__pk=institution)
			institut = Institution.objects.get(pk=institution)
			print'iiiiiiiiiiiii',institut

			
			data=[]
			for village in villages:
				d={}
				d['pk']=village.pk
				d['village_name']=village.village
				d['latitude']=str(institut.latitude)
				d['longitude']=str(institut.longitude)
				data.append(d)

			print'data--',data
		else:
			farmers = FarmerDetail.objects.filter(village__pk=village)

			data=[]
			audited=[]

			for farmer in farmers:
				fields= FarmerField.objects.filter(farmerDetails__pk=farmer.pk)
				d={}
				d['pk']=farmer.pk
				d['name']=farmer.name
			
				field_count = len(fields)
				if len(fields) == 0:
					field_count = -1
				if farmer.no_of_fields > field_count:
					data.append(d)
				
				else:
					audited.append(d)
			
		return HttpResponse(content=json.dumps(data),content_type='application/json')

def register(request):
	if request.method == "GET":
		institution = Institution.objects.all()
		
		return render(request, "register.html",{'institution':institution})

	elif request.is_ajax(): 
		institution = request.POST.get('institution')
		print'eeeeeeeee',institution
		
		villages = Village.objects.filter(institution__pk=institution)
		institut = Institution.objects.get(pk=institution)
		print'iiiiiiiiiiiii',villages

		
		data=[]
		for village in villages:
			d={}
			d['pk']=village.pk
			d['village_name']=village.village
			data.append(d)

		# print'data--',data
	        return HttpResponse(content=json.dumps(data),content_type='application/json')

	elif request.method == "POST":
		name = request.POST.get('name')
		mobile_num = request.POST.get('phonenumber')
		district = request.POST.get('district')
		state = request.POST.get('state')
		farmer_code = request.POST.get('farmercode')
		no_of_fields = request.POST.get('no_of_fields')
		
		db_register = FarmerDetail.objects.create(
			name=name,
			mobile_num = mobile_num,
			district = district,
			state = state,
			farmer_code = farmer_code,
			no_of_fields = no_of_fields,
			institution = Institution.objects.get(pk=request.POST.get('institution')),
			village = Village.objects.get(pk=request.POST.get('villpk')),
			)
 		
		return HttpResponseRedirect('/register/')

def add_institution(request):
	if request.method == "GET":
		return render(request, "add_institution.html")

	elif request.method =='POST':
		institution_name = request.POST.get('institution_name')
		address = request.POST.get('address')
		phone_no = request.POST.get('phonenumber')
		latitude = request.POST.get('latitude')
		longitude = request.POST.get('longitude')
		db_institution = Institution.objects.create(
			institution_name =institution_name,
			address = address,
			phone_no = phone_no,
			latitude = latitude,
			longitude = longitude,
			)
		return HttpResponseRedirect('/add_institution/')


def add_crop(request):
	if request.method == "GET":
		return render(request, "add_crop.html")

	elif request.method =='POST':
		crop_name = request.POST.get('crop_name')

		db_crop = Crop.objects.create(
				crop_name = crop_name,
				)
		return HttpResponseRedirect('/add_crop/')


def add_village(request):
	if request.method == "GET":
		institution = Institution.objects.all()
		return render(request,"add_village.html",{'institution':institution})

	elif request.method == "POST":
		village = request.POST.get('village')
		db_village=Village.objects.create(
		village = village,
		institution = Institution.objects.get(pk=request.POST.get('institution')),

		)
		return HttpResponseRedirect('/add_village/')









def data(request):
	if request.method == "GET":
		farmers = FarmerDetail.objects.all()
		farmerfield = FarmerField.objects.all()
		return render(request, "map.html",{'farmers':farmers,'farmerfield':farmerfield})
	elif request.method =='POST':
		farmers = FarmerDetail.objects.all()
		farmerfield = FarmerField.objects.all()
		lat = request.POST.getlist('latitudes')
		lng = request.POST.getlist('longitudes')
		db_farmerfield = FarmerField.objects.create(
			acars = request.POST.get('acres'),
			distance_from_institution = request.POST.get('distance1'),
			town = "0" , 
			farmerDetails = FarmerDetail.objects.get(pk = request.POST.get('farmerselect')),
			crop_name=Crop.objects.get(pk = request.POST.get('crop')),
			water_source=Water_source.objects.get(pk = request.POST.get('watersource')),
			village=Village.objects.get(pk=request.POST.get('villpk'))
			)
		
		for lat1,lng1 in zip(lat,lng):
			d=[]
			db_points = Point.objects.create(latitude = lat1,longitude = lng1 , farmerField = db_farmerfield)
			d.append(db_points)
			for i in d:
				la=i.latitude
				ln=i.longitude
	return HttpResponseRedirect('/map/')


# def members(request):
# 	if request.method == "GET":
# 		return render(request,"/")
# 	elif request.method == "POST":
# 		name = request.POST.get()

def demo(request):
	if request.method == "GET":	

		wb = xlrd.open_workbook('/home/shruthi/Documents/farmerdetails.xls',on_demand = True)
		sheet = wb.sheet_by_index(0)
		for i in xrange(sheet.nrows):
		    values = []
		    print "jjjjjjjjjjjjjjjjjj",str(sheet.row(i)[0].value)
		    db_farmer = FarmerDetail.objects.create(name = str(sheet.row(i)[0].value),
		    	mobile_num =  str(sheet.row(i)[3].value),
		    	# village =  str(sheet.row(i)[4].value),
		    	district =  str(sheet.row(i)[6].value),
		    	farmer_code =  str(sheet.row(i)[1].value))
		    
		return render(request,'login.html')

def report(request,pk):
	if request.method == "GET":

		farmers = FarmerDetail.objects.filter(village__pk=pk)
		total_farmers=len(farmers)
		
		d=[]
		di=[]
		audited=[]
		aud_length=0
		notaudited_length=0
		field=0
		
		for farmer in farmers:
			fields= FarmerField.objects.filter(farmerDetails__pk = farmer.pk)
			ac = FarmerField.objects.filter(farmerDetails__pk = farmer.pk).aggregate(Sum('acars'))
			print'oooooooooooooo----------',ac
			audited.append(ac)
			# for i in fields:
			# 	n=i.acars
			# 	print'kkkkkkkk',n
			# print'kkkkkkkkkkkkk.......>',k

			field_count=0
			field_count = len(fields)
			
			if len(fields) == 0:
				field_count = -1

			if farmer.no_of_fields > field_count:
				
				field= farmer.no_of_fields-len(fields)
				print'-----field:',field
			
				dic={}
				dic['name']=farmer.name
				dic['farmer_code']=farmer.farmer_code
				dic['village']=farmer.village.village
				dic['district']=farmer.district
				dic['field']=field

				d.append(dic)
				
				notaudited_length=len(d)
		
			else:
				audited.append(farmer)				
				print'aud------------',audited
				aud_length=len(audited)
			
		return render(request,'report.html',{'farmers':farmers,'notaudited':d,'audited':audited,'total_farmers':total_farmers,'aud_length':aud_length,'notaudited_length':notaudited_length,'field':field,'ac':ac})
	

def village(request,pk):
	if request.method == "GET":
		villages=Village.objects.filter(institution__pk=pk)
		farmers = FarmerDetail.objects.filter(village__pk=pk)
		tot_farmers=len(farmers)

		d=[]
		di=[]
		audited=[]
		aud_length=0
		notaudited_length=0
		field=0
		
		for farmer in farmers:
			fields= FarmerField.objects.filter(farmerDetails__pk = farmer.pk)
			print'fiel:--------------',fields

			field_count=0
			field_count = len(fields)
			
			if len(fields) == 0:
				field_count = -1

			if farmer.no_of_fields > field_count:
				
				field= farmer.no_of_fields-len(fields)
			
				dic={}
				dic['name']=farmer.name
				dic['farmer_code']=farmer.farmer_code
				dic['village']=farmer.village.village
				dic['district']=farmer.district
				dic['field']=field

				d.append(dic)
				
				notaudited_length=len(d)
		
			else:
				audited.append(farmer)
				aud_length=len(audited)


		
	
		return render(request,'village.html',{'village':villages,'aud_length':aud_length,'notaudited_length':notaudited_length,'tot_farmers':tot_farmers})

def institution(request):
	if request.method == "GET":
		user_pk=request.user.pk
		print'xxxx:---------------->',user_pk
		institution_name = Institution.objects.all()

		tot_village=[]
		vil=0
		data=[]
		member = Member.objects.get(profile__pk=user_pk)
		print'+++++++++',member.institution.all()
		institutions = member.institution.all()
		# institution = Institution.objects.filter(pk=member.institution.pk)
		for institution in institutions:
			d={}
			d['pk']=institution.pk
			d['institution']=institution.institution_name
			d['address'] = institution.address
			d['phone_no'] = institution.phone_no
			
			villages = Village.objects.filter(institution__pk=institution.pk)
			mem = Member.objects.filter(institution__pk=institution.pk)
			print'mmmmm----------------->',member
			tot_village.append(villages)
			vil=len(villages)
			d['vil']=vil

			data.append(d)
		print'dddddd',data

			

		
		return render(request,'institution.html',{'data':data,'mem':mem})
