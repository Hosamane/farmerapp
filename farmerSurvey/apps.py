from django.apps import AppConfig


class FarmersurveyConfig(AppConfig):
    name = 'farmerSurvey'
