from django.conf.urls import url
from django.contrib import admin

from . import views
app_name = 'farmerSurvey'
urlpatterns = [
	url(r'^map/$', views.mapview),
	url(r'^data/$', views.data),
	url(r'^register/$',views.register),
	url(r'^demo/$', views.demo),
	url(r'^$', views.login),
	url(r'^$', views.logout_view),
	url(r'^report/(?P<pk>[\w{}.-]{1,40})$', views.report),	
	url(r'^village/(?P<pk>[\w{}.-]{1,40})$', views.village),
	url(r'^institution/$', views.institution),
	url(r'^add_institution/$', views.add_institution),
	url(r'^add_crop/$', views.add_crop),
	url(r'^add_village/$', views.add_village),
	
]